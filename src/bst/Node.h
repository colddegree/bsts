#ifndef BSTS_NODE_H
#define BSTS_NODE_H


struct Node {
    const int key;
    Node *left = nullptr;
    Node *right = nullptr;
    Node *parent = nullptr;

    explicit Node(int key) : key(key) {}
};


#endif //BSTS_NODE_H
