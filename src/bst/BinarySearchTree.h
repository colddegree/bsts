#ifndef BSTS_BINARYSEARCHTREE_H
#define BSTS_BINARYSEARCHTREE_H


#include <iostream>
#include <iomanip>
#include "../IBinarySearchTree.h"
#include "Node.h"

using std::cout;
using std::endl;
using std::setw;

class BinarySearchTree : public IBinarySearchTree {
public:
    bool contains(int key) override {
        return find(key) != nullptr;
    }

    void insert(int key) override {
        Node *curr = root;
        Node *parentOfCurr = nullptr;

        while (curr != nullptr) {
            parentOfCurr = curr;
            if (key < curr->key)
                curr = curr->left;
            else
                curr = curr->right;
        }

        Node *insertableNode = new Node(key);
        insertableNode->parent = parentOfCurr;

        if (parentOfCurr == nullptr) // if tree was empty
            root = insertableNode;
        else if (key < parentOfCurr->key)
            parentOfCurr->left = insertableNode;
        else
            parentOfCurr->right = insertableNode;
    }

    // TODO: unreadable
    void remove(int key) override {
        Node *removableNode = find(key);

        if (removableNode == nullptr)
            return;

        if (removableNode->left == nullptr) {
            transplant(removableNode, removableNode->right);
        } else if (removableNode->right == nullptr) {
            transplant(removableNode, removableNode->left);
        } else {
            Node *successorNode = min(removableNode->right);

            if (successorNode->parent != removableNode) {
                transplant(successorNode, successorNode->right);
                successorNode->right = removableNode->right;
                successorNode->right->parent = successorNode;
            }

            transplant(removableNode, successorNode);
            successorNode->left = removableNode->left;
            successorNode->left->parent = successorNode;
        }
    }

    ~BinarySearchTree() override {
        deleteNodeRecursively(root);
    }

    /**
     * Выводит дерево в стандартный поток вывода, начиная с крайнего правого узла (печать Вирта)
     */
    void print() override {
        print(root, 0);
    }

private:
    /**
     * Ищет узел с ключом key
     * @param key
     * @return указатель на узел с ключом key
     */
    Node *find(int key) {
        Node *curr = root;

        while (curr != nullptr && key != curr->key) {
            if (key < curr->key)
                curr = curr->left;
            else
                curr = curr->right;
        }

        return curr;
    }

    /**
     * Заменяет поддерево u поддеревом v.
     * Родитель u становится родителем v, v становится соответствующим дочерним узлом
     * родительского по отношению к u узла.
     * @param u указатель на поддерево
     * @param v указатель на поддерево
     */
    void transplant(Node *u, Node *v) {
        if (u->parent == nullptr)
            root = v;
        else if (u == u->parent->left)
            u->parent->left = v;
        else
            u->parent->right = v;

        if (v != nullptr)
            v->parent = u->parent;
    }

    /**
     * Ищет узел в поддереве subtree с минимальным ключом
     * @param subtree указатель на поддерево
     * @return указатель на узел с минимальным ключом
     */
    Node *min(Node *subtree) {
        Node *curr = subtree;

        while (curr->left != nullptr)
            curr = curr->left;

        return curr;
    }

    /**
     * Рекурсивно удаляет node и его потомков
     * @param node указатель на узел
     */
    void deleteNodeRecursively(Node *node) {
        if (node == nullptr)
            return;

        deleteNodeRecursively(node->left);
        deleteNodeRecursively(node->right);

        delete node;
        node = nullptr;
    }


    void print(Node *node, int depth) {
        if (node == nullptr)
            return;

        print(node->right, depth + 1);

        for (int i = 0; i < depth; ++i)
            cout << "\t";
        static const int WIDTH = 4;
        cout << setw(WIDTH) << node->key << endl;

        print(node->left, depth + 1);
    }


    Node *root = nullptr;
};


#endif //BSTS_BINARYSEARCHTREE_H
