#ifndef BSTS_RBNODE_T_HPP
#define BSTS_RBNODE_T_HPP


template <class V>
RBNode<V>::RBNode(int key, V value, Color color)
        : key(key)
        , value(value)
        , color(color)
{
    left = right = parent = null;
}

template <class V>
RBNode<V>::RBNode(const RBNode *other) : RBNode(other->key, other->value, other->color) {
    if (other == null)
        return;

    if (other->left != null) {
        left = new RBNode(other->left);
        left->parent = this;
    }

    if (other->right != null) {
        right = new RBNode(other->right);
        right->parent = this;
    }
}

template <class V>
bool RBNode<V>::isLeaf() const {
    return this != null &&
           left == null &&
           right == null;
}

template <class V>
bool RBNode<V>::isLeftChild() const {
    return this != null &&
           this == parent->left;
}

template <class V>
bool RBNode<V>::isRightChild() const {
    return this != null &&
           this == parent->right;
}

template <class V>
bool RBNode<V>::isRed() const {
    return color == RED;
}

template <class V>
bool RBNode<V>::isBlack() const {
    return color == BLACK;
}

template <class V>
void RBNode<V>::deleteRecursively() {
    deleteRecursively(this);
}

template <class V>
RBNode<V> *RBNode<V>::replaceWith(RBNode *newNode) {
    newNode->parent = parent;
    newNode->left = left;
    newNode->right = right;

    if (newNode->left != null)
        newNode->left->parent = newNode;

    if (newNode->right != null)
        newNode->right->parent = newNode;

    if (newNode->parent != null) {
        if (newNode->parent->left == this)
            newNode->parent->left = newNode;
        else
            newNode->parent->right = newNode;
    }

    delete this;
    return newNode;
}

template <class V>
void RBNode<V>::deleteRecursively(RBNode *node) {
    if (node == null)
        return;

    deleteRecursively(node->left);
    deleteRecursively(node->right);

    if (node == node->parent->left)
        node->parent->left = null;
    else
        node->parent->right = null;

    delete node;
    node = nullptr;
}


#endif //BSTS_RBNODE_T_HPP
