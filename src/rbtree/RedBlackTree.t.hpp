#ifndef BSTS_REDBLACKTREE_T_HPP
#define BSTS_REDBLACKTREE_T_HPP


#define DEBUG

#ifndef DEBUG
#define NDEBUG
#endif
#include <cassert>


#include <functional>
#include <set>
#include <iostream>
#include <iomanip>


#define RED node_type::RED
#define BLACK node_type::BLACK
#define null node_type::null


template <class V>
RedBlackTree<V>::RedBlackTree(const RedBlackTree& other) {
    if (other.root != null)
        this->root = new node_type(other.root);
    else
        this->root = null;
}

template <class V>
RedBlackTree<V>::RedBlackTree(RedBlackTree&& other) noexcept {
    this->root = other.root;
    other.root = nullptr;
}

template <class V>
RedBlackTree<V>& RedBlackTree<V>::operator=(const RedBlackTree& other) {
    RedBlackTree tmp(other);
    swap(*this, tmp);
    return *this;
}

template <class V>
RedBlackTree<V>& RedBlackTree<V>::operator=(RedBlackTree&& other) noexcept {
    swap(*this, other);
    return *this;
}

template <class V>
void RedBlackTree<V>::swap(RedBlackTree<V>& lhs, RedBlackTree<V>& rhs) {
    using std::swap;
    swap(lhs.root, rhs.root);
}

template <class V>
RedBlackTree<V>::~RedBlackTree() {
    root->deleteRecursively();
}

template <class V>
bool RedBlackTree<V>::contains(int key) const {
    return find(key) != null;
}

template <class V>
void RedBlackTree<V>::insert(int key, V value) {
    auto *oldNode = find(key);

    if (oldNode != null) {
        auto *insertedNode = new node_type{ key, value, oldNode->color };

        bool oldNodeWasRoot = false;
        if (oldNode == root)
            oldNodeWasRoot = true;

        oldNode->replaceWith(insertedNode);

        if (oldNodeWasRoot)
            root = insertedNode;

        isOk();
        return;
    }

    node_type *curr = root;
    node_type *parentOfCurr = null;

    while (curr != null) {
        parentOfCurr = curr;
        if (key < curr->key)
            curr = curr->left;
        else
            curr = curr->right;
    }

    auto *insertedNode = new node_type{ key, value, RED };
    insertedNode->parent = parentOfCurr;

    if (parentOfCurr == null) // if tree was empty
        root = insertedNode;
    else if (key < parentOfCurr->key)
        parentOfCurr->left = insertedNode;
    else
        parentOfCurr->right = insertedNode;

    insertFixup(insertedNode);

    isOk();
}

template <class V>
void RedBlackTree<V>::insert(int key) {
    insert(key, V());
}

template <class V>
void RedBlackTree<V>::remove(int key) {
    node_type *removed = find(key); // z -- удаляемый узел

    if (removed == null)
        return;

    node_type *y = removed; // y -- удаляемый узел, если потомков < 2,
                            // иначе -- узел, следующий за удаляемым, перемещённый на его место
    typename node_type::Color yOriginalColor = y->color;

    node_type *patchForY = null; // x -- узел, перемещаемый в исходную позицию y

    if (removed->left == null) {
        patchForY = removed->right;
        transplant(removed, removed->right);
    } else if (removed->right == null) {
        patchForY = removed->left;
        transplant(removed, removed->left);
    } else {
        y = min(removed->right);
        yOriginalColor = y->color;

        patchForY = y->right;

        if (y->parent == removed)
            patchForY->parent = y;
        else {
            transplant(y, y->right);
            y->right = removed->right;
            y->right->parent = y;
        }

        transplant(removed, y);

        y->left = removed->left;
        y->left->parent = y;
        y->color = removed->color;
    }

    delete removed;

    if (yOriginalColor == BLACK)
        removeFixup(patchForY);

    isOk();
}

template <class V>
V& RedBlackTree<V>::valueAt(int key) const {
    auto *node = find(key);

    if (node == null)
        throw std::invalid_argument("Node with key=" + std::to_string(key) + " not found.");

    return node->value;
}

template<class V>
bool RedBlackTree<V>::isEmpty() const {
    return root == null;
}

template<class V>
unsigned int RedBlackTree<V>::size() const {
    unsigned int result = 0;

    for (auto pair : *this)
        ++result;

    return result;
}

template <class V>
bool RedBlackTree<V>::operator==(const RedBlackTree &rhs) {
    return root == rhs.root;
}

template <class V>
void RedBlackTree<V>::print() const {
    print(root, 0);
}

template <class V>
bool RedBlackTree<V>::isOk() {
    bool status = true;
#ifdef DEBUG
    status = checkInvariants();
#endif
    return status;
}

// iterator

template <class V>
RedBlackTree<V>::Iterator::Iterator(const RedBlackTree *instance, node_type *initialNode)
    : instance(instance)
    , currentNode(initialNode) {}

template <class V>
typename RedBlackTree<V>::Iterator RedBlackTree<V>::Iterator::operator++() {
    if (currentNode == null)
        return *this;

    if (currentNode->right == null) {
        // пока не перейдём по / пути
        while (true) {
            auto parentNode = currentNode->parent;

            if (parentNode == null) {
                currentNode = null;
                return *this;
            }

            bool passedThroughLeftPath = false;

            if (currentNode->parent->left == currentNode)
                passedThroughLeftPath = true;

            currentNode = parentNode;

            if (passedThroughLeftPath)
                break;
        }
    } else {
        currentNode = instance->min(currentNode->right);
    }

    return *this;
}

template <class V>
std::pair<int, V&> RedBlackTree<V>::Iterator::operator*() const {
    return std::pair<int, V&>{ currentNode->key, currentNode->value };
}

template <class V>
bool RedBlackTree<V>::Iterator::operator==(const RedBlackTree::Iterator& rhs) const {
    return currentNode == rhs.currentNode;
}

template <class V>
bool RedBlackTree<V>::Iterator::operator!=(const RedBlackTree::Iterator& rhs) const {
    return !operator==(rhs);
}

template <class V>
typename RedBlackTree<V>::Iterator RedBlackTree<V>::begin() const {
    return Iterator{ this, min(root) };
}

template <class V>
typename RedBlackTree<V>::Iterator RedBlackTree<V>::end() const {
    return Iterator{ this, null };
}

// end of iterator

template <class V>
std::pair<int, V&> RedBlackTree<V>::front() const {
    auto *node = min(root);
    return pair_type{ node->key, node->value };
}

template <class V>
std::pair<int, V&> RedBlackTree<V>::back() const {
    auto *node = max(root);
    return pair_type{ node->key, node->value };
}

template <class V>
RBNode<V> *RedBlackTree<V>::find(int key) const {
    node_type *curr = root;

    while (curr != null && key != curr->key) {
        if (key < curr->key)
            curr = curr->left;
        else
            curr = curr->right;
    }

    return curr;
}

template <class V>
void RedBlackTree<V>::insertFixup(node_type *node) {
    while (node->parent->isRed()) {
        node_type *uncle;

        if (node->parent->isLeftChild()) {

            uncle = node->parent->parent->right;

            if (uncle->isRed()) {
                // CASE 1: Uncle of insertable node is red
                node->parent->color = BLACK;
                uncle->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            }
            else {
                if (node->isRightChild()) {
                    // CASE 2: Uncle of insertable node is black && insertable node is right child
                    node = node->parent;
                    leftRotate(node);
                }

                // CASE 3: Uncle of insertable node is black && insertable node is left child
                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                rightRotate(node->parent->parent);
            }

        }

            // same thing, but replacing the right with the left and vice versa
        else {

            uncle = node->parent->parent->left;

            if (uncle->isRed()) {
                node->parent->color = BLACK;
                uncle->color = BLACK;
                node->parent->parent->color = RED;
                node = node->parent->parent;
            }
            else {
                if (node->isLeftChild()) {
                    node = node->parent;
                    rightRotate(node);
                }
                node->parent->color = BLACK;
                node->parent->parent->color = RED;
                leftRotate(node->parent->parent);
            }

        }

    }

    root->color = BLACK;
}

template <class V>
void RedBlackTree<V>::leftRotate(node_type *node) {
#ifdef DEBUG
    assert(node->right != null);
    assert(root->parent == null);
#endif

    node_type *rightChild = node->right;
    node->right = rightChild->left;

    if (rightChild->left != null)
        rightChild->left->parent = node;

    rightChild->parent = node->parent;

    if (node->parent == null)
        root = rightChild;
    else if (node == node->parent->left)
        node->parent->left = rightChild;
    else
        node->parent->right = rightChild;

    rightChild->left = node;
    node->parent = rightChild;
}

template <class V>
void RedBlackTree<V>::rightRotate(node_type *node) {
#ifdef DEBUG
    assert(node->left != null);
    assert(root->parent == null);
#endif

    node_type *leftChild = node->left;
    node->left = leftChild->right;

    if (leftChild->right != null)
        leftChild->right->parent = node;

    leftChild->parent = node->parent;

    if (node->parent == null)
        root = leftChild;
    else if (node == node->parent->left)
        node->parent->left = leftChild;
    else
        node->parent->right = leftChild;

    leftChild->right = node;
    node->parent = leftChild;
}

template <class V>
void RedBlackTree<V>::removeFixup(node_type *x) {
    while (x != root && x->color == BLACK) {

        if (x == x->parent->left) {
            node_type *siblingOfX = x->parent->right;

            if (siblingOfX->color == RED) {
                // CASE 1: Sibling of x is red
                siblingOfX->color = BLACK;
                x->parent->color = RED;
                leftRotate(x->parent);
                siblingOfX = x->parent->right;
            }

            if (siblingOfX->left->color == BLACK && siblingOfX->right->color == BLACK) {
                // CASE 2: Sibling of x is black && both of its children are black
                siblingOfX->color = RED;
                x = x->parent;
            }
            else {
                if (siblingOfX->right->color == BLACK) {
                    // CASE 3: Sibling of x is black && its left child is red, but right child is black
                    siblingOfX->left->color = BLACK;
                    siblingOfX->color = RED;
                    rightRotate(siblingOfX);
                    siblingOfX = x->parent->right;
                }

                // CASE 4: Sibling of x is black && its right child is red
                siblingOfX->color = x->parent->color;
                x->parent->color = BLACK;
                siblingOfX->right->color = BLACK;
                leftRotate(x->parent);
                x = root;
            }

        }

            // same thing, but replacing the right with the left and vice versa
        else {
            node_type *siblingOfX = x->parent->left;

            if (siblingOfX->color == RED) {
                siblingOfX->color = BLACK;
                x->parent->color = RED;
                rightRotate(x->parent);
                siblingOfX = x->parent->left;
            }

            if (siblingOfX->right->color == BLACK && siblingOfX->left->color == BLACK) {
                siblingOfX->color = RED;
                x = x->parent;
            }
            else {
                if (siblingOfX->left->color == BLACK) {
                    siblingOfX->right->color = BLACK;
                    siblingOfX->color = RED;
                    leftRotate(siblingOfX);
                    siblingOfX = x->parent->left;
                }

                siblingOfX->color = x->parent->color;
                x->parent->color = BLACK;
                siblingOfX->left->color = BLACK;
                rightRotate(x->parent);
                x = root;
            }

        }

    }

    x->color = BLACK;
}

template <class V>
void RedBlackTree<V>::transplant(node_type *u, node_type *v) {
    if (u->parent == null)
        root = v;
    else if (u == u->parent->left)
        u->parent->left = v;
    else
        u->parent->right = v;

    v->parent = u->parent;
}

template <class V>
RBNode<V> *RedBlackTree<V>::min(node_type *subtree) const {
    if (subtree == null)
        return subtree;

    node_type *curr = subtree;

    while (curr->left != null)
        curr = curr->left;

    return curr;
}

template <class V>
RBNode<V> *RedBlackTree<V>::max(node_type *subtree) const {
    if (subtree == null)
        return subtree;

    node_type *curr = subtree;

    while (curr->right != null)
        curr = curr->right;

    return curr;
}

// assertions

template <class V>
bool RedBlackTree<V>::checkInvariants() {
    assertThatThisTreeIsBSTWithoutDuplicates();

    assertThatEveryNodeIsRedOrBlack();
    assertThatRootIsBlack();
    assertThatNullIsBlack();
    assertThatRedsChildrenAreBlack();
    assertThatForEachNodeAllRoutesToLeafsContainSameAmountOfBlackNodes();

    return true;
}

template <class V>
void RedBlackTree<V>::assertThatThisTreeIsBSTWithoutDuplicates() {

    std::function<void(node_type*)> check = [&](node_type *node) {
        if (node == null)
            return;

        check(node->left);
        check(node->right);

        if (node->left != null)
            assert(node->left->key < node->key);

        if (node->right != null)
            assert(node->right->key > node->key);
    };

    check(root);
}

template <class V>
void RedBlackTree<V>::assertThatEveryNodeIsRedOrBlack() {

    std::function<void(node_type*)> check = [&](node_type *node) {
        if (node == null)
            return;

        check(node->left);
        check(node->right);

        assert(node->color == RED ||
               node->color == BLACK);
    };

    check(root);
}

template <class V>
void RedBlackTree<V>::assertThatRootIsBlack() {
    assert(root->color == BLACK);
}

template <class V>
void RedBlackTree<V>::assertThatNullIsBlack() {
    assert(null->color == BLACK);
}

template <class V>
void RedBlackTree<V>::assertThatRedsChildrenAreBlack() {

    std::function<void(node_type*)> check = [&](node_type* node) {
        if (node == null)
            return;

        check(node->left);
        check(node->right);

        if (node->color == RED) {
            assert(node->left->color == BLACK);
            assert(node->right->color == BLACK);
        }
    };

    check(root);
}

template <class V>
void RedBlackTree<V>::assertThatForEachNodeAllRoutesToLeafsContainSameAmountOfBlackNodes() {

    std::function<void(node_type*)> check = [&](node_type *node) {
        if (node == null)
            return;

        check(node->left);
        check(node->right);

        assertThatAllRoutesOfSpecifiedNodeToLeafsContainSameAmountOfBlackNodes(node);
    };

    check(root);
}

template <class V>
void RedBlackTree<V>::assertThatAllRoutesOfSpecifiedNodeToLeafsContainSameAmountOfBlackNodes(node_type *node) {
    std::set<int> blackHeights;

    std::function<void(node_type*, int)> check = [&](node_type *node, int blackNodesOnCurrRoute) {
        if (node == null)
            return;

        int isBlack = (node->color == BLACK) ? 1 : 0;
        blackNodesOnCurrRoute += isBlack;

        check(node->left, blackNodesOnCurrRoute);
        check(node->right, blackNodesOnCurrRoute);

        if (node->isLeaf())
            blackHeights.insert(blackNodesOnCurrRoute);
    };

    check(node, 0);

    assert(blackHeights.size() == 1);
}

// end of assertions

template <class V>
void RedBlackTree<V>::print(node_type *node, int depth) const {
    using std::cout;
    using std::endl;
    using std::setw;

    if (node == null)
        return;

    print(node->right, depth + 1);


    for (int i = 0; i < depth; ++i)
        cout << "       ";

    char color = 'U';
    if (node->color == RED)
        color = 'R';
    else if (node->color == BLACK)
        color = 'B';

    static const int WIDTH = 4;
    cout
            << setw(WIDTH)
            << node->key
            << '('
            << color
            << ')'
            << endl;


    print(node->left, depth + 1);
}


#endif //BSTS_REDBLACKTREE_T_HPP
