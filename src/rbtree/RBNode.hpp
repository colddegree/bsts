#ifndef BSTS_RBNODE_HPP
#define BSTS_RBNODE_HPP


template <class V>
struct RBNode {
    const int key;
    V value;
    RBNode *left, *right, *parent;

    enum Color { RED, BLACK, UNINITIALIZED };
    Color color = UNINITIALIZED;


    static RBNode *null;


    RBNode(int key, V value, Color color);
    RBNode(const RBNode *other);

    bool isLeaf() const;
    bool isLeftChild() const;
    bool isRightChild() const;

    bool isRed() const;
    bool isBlack() const;

    /**
     * Рекурсивно удаляет самого себя и своих потомков
     */
    void deleteRecursively();

    /**
     * Ставит узел newNode на место текущего узла (связывает новый узел указателями старого)
     * @param newNode новый узел
     * @return указатель на новый узел
     */
    RBNode *replaceWith(RBNode *newNode);

private:
    void deleteRecursively(RBNode *node);
};

template <class V>
RBNode<V> *RBNode<V>::null = new RBNode{ 0, V(), RBNode::BLACK };

// implementation
#include "RBNode.t.hpp"


#endif //BSTS_RBNODE_HPP
