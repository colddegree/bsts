#ifndef BSTS_REDBLACKTREE_HPP
#define BSTS_REDBLACKTREE_HPP


#include <utility>
#include "../IBinarySearchTree.h"
#include "RBNode.hpp"

/**
 * Красно-чёрное дерево без дублирующихся ключей
 * @tparam V тип значения, хранящегося в узлах дерева
 */
template <class V = int>
class RedBlackTree : public IBinarySearchTree<V> {
public:
    typedef RBNode<V> node_type;
    typedef std::pair<int, V&> pair_type;


    RedBlackTree() = default;
    RedBlackTree(const RedBlackTree& other);
    RedBlackTree(RedBlackTree&& other) noexcept;

    RedBlackTree& operator=(const RedBlackTree& other);
    RedBlackTree& operator=(RedBlackTree&& other) noexcept;

    void swap(RedBlackTree& lhs, RedBlackTree& rhs);

    ~RedBlackTree() override;


    bool contains(int key) const override;
    void insert(int key, V value) override;
    void insert(int key) override;
    void remove(int key) override;
    V& valueAt(int key) const override;
    bool isEmpty() const override;


    unsigned int size() const;

    bool operator==(const RedBlackTree& rhs);

    void print() const override;


    bool isOk();


    // in-order traverse iterator
    class Iterator {
    public:
        Iterator(const RedBlackTree *instance, node_type *initialNode);

        Iterator operator++();
        pair_type operator*() const;

        bool operator==(const Iterator& rhs) const;
        bool operator!=(const Iterator& rhs) const;
    private:
        const RedBlackTree *instance;
        node_type *currentNode;
    };

    Iterator begin() const;
    Iterator end() const;

    pair_type front() const;
    pair_type back() const;


private:
    /**
     * Ищет узел с ключом key
     * @param key
     * @return указатель на узел с ключом key
     */
    node_type *find(int key) const;

    void insertFixup(node_type *node);

    void leftRotate(node_type *node);

    void rightRotate(node_type *node);

    void removeFixup(node_type *x);

    /**
     * Заменяет поддерево u поддеревом v.
     * Родитель u становится родителем v, v становится соответствующим дочерним узлом
     * родительского по отношению к u узла.
     * @param u указатель на поддерево
     * @param v указатель на поддерево
     */
    void transplant(node_type *u, node_type *v);

    /**
     * Ищет узел в поддереве subtree с минимальным ключом
     * @param subtree указатель на поддерево
     * @return указатель на узел с минимальным ключом
     */
    node_type *min(node_type *subtree) const;

    /**
     * Ищет узел в поддереве subtree с максимальным ключом
     * @param subtree указатель на поддерево
     * @return указатель на узел с максимальным ключом
     */
    node_type *max(node_type *subtree) const;


    bool checkInvariants();

    void assertThatThisTreeIsBSTWithoutDuplicates();

    void assertThatEveryNodeIsRedOrBlack();

    void assertThatRootIsBlack();

    void assertThatNullIsBlack();

    void assertThatRedsChildrenAreBlack();

    void assertThatForEachNodeAllRoutesToLeafsContainSameAmountOfBlackNodes();

    void assertThatAllRoutesOfSpecifiedNodeToLeafsContainSameAmountOfBlackNodes(node_type *node);


    void print(node_type *node, int depth) const;


    node_type *root = node_type::null;
};


// implementation
#include "RedBlackTree.t.hpp"


#endif //BSTS_REDBLACKTREE_HPP
