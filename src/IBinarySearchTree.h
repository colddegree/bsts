#ifndef BSTS_IBINARYSEARCHTREE_H
#define BSTS_IBINARYSEARCHTREE_H


template <class V = int>
class IBinarySearchTree {
public:
    /**
     * Проверяет наличие узла с ключом key в дереве
     * @param key
     * @return true если содержит, иначе false
     */
    virtual bool contains(int key) const = 0;

    /**
     * Добавляет в дерево узел с ключом key и значением value
     * @param key
     * @param value
     */
    virtual void insert(int key, V value) = 0;

    /**
     * Добавляет в дерево узел с ключом key и значением value по умолчанию
     * @param key
     */
    virtual void insert(int key) = 0;

    /**
     * Удаляет из дерева узел с ключом key
     * @param key
     */
    virtual void remove(int key) = 0;

    /**
     * Возвращает ссылку на значение узла с ключом key
     * @param key
     * @return значения узла
     */
    virtual V& valueAt(int key) const = 0;

    /**
     * Проверяет дерево на пустоту
     * @return true, если дерево пусто, иначе false
     */
    virtual bool isEmpty() const = 0;

    virtual ~IBinarySearchTree() = default;

    /**
     * Выводит дерево в стандартный поток вывода
     */
    virtual void print() const = 0;
};


#endif //BSTS_IBINARYSEARCHTREE_H
