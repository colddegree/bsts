#include <iostream>
#include "rbtree/RedBlackTree.hpp"

using namespace std;

int main() {
    RedBlackTree<long> tree;

    for (int i = 0; i < 8; ++i)
        tree.insert(i, i);
    tree.insert(4, 4);
    tree.insert(4, 4);

    tree.print();

    for (auto it = tree.begin(); it != tree.end(); ++it) {
        cout << (*it).second << endl;
    }

    cout << endl;

    for (auto item : tree) {
        cout << item.first << ": " << item.second << endl;
    }

    cout << endl;

    cout << tree.valueAt(3) << endl;
//    cout << tree.valueAt(10) << endl;


    RedBlackTree<string> strTree;
    strTree.insert(1, "hi");
    strTree.insert(2, "there");

    for (auto item : strTree)
        cout << item.first << ": " << item.second << endl;

    cout << strTree.front().second << endl;
    cout << strTree.back().second << endl;

    cout << endl;

    RedBlackTree<RedBlackTree<int>> t;
    t.insert(1);
    t.print();

    t.valueAt(1).insert(1);
    t.valueAt(1).insert(2);
    t.valueAt(1).insert(3);

    cout << endl;

    t.valueAt(1).print();

    for (auto pair_i : t) {
        auto key = pair_i.first;
        auto value = pair_i.second;

        cout << key << ": ";

        auto it = value.begin();
        while (it != value.end()) {
            auto smth = (*it).first;
            cout << smth << ", ";
            ++it;
        }

        cout << endl;
    }

    return 0;
}