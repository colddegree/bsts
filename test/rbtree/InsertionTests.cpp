#include <gtest/gtest.h>
#include "../../src/rbtree/RedBlackTree.hpp"


TEST(InsertionTests, InsertableNodeIsLeftChild) {
    RedBlackTree<int> tree;

    int keys[] = { 11, 2, 14, 1, 7, 15, 5, 8 };

    for (int key : keys)
        tree.insert(key);

    tree.insert(4);

    ASSERT_TRUE(tree.isOk());
}

TEST(InsertionTests, InsertableNodeIsRightChild) {
    RedBlackTree<int> tree;

    int keys[] = { 11, 6, 20, 3, 15, 22, 12, 17 };

    for (int key : keys)
        tree.insert(key);

    tree.insert(19);

    ASSERT_TRUE(tree.isOk());
}
