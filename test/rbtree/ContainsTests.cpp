#include <gtest/gtest.h>
#include "../../src/IBinarySearchTree.h"
#include "../../src/rbtree/RedBlackTree.hpp"


class ContainsTests : public ::testing::Test {
protected:
    IBinarySearchTree<int> *tree = nullptr;

    void SetUp() override {
        tree = new RedBlackTree<int>;
        initTree();
    }

    void initTree() {
        for (int i = 0; i < 8; ++i)
            tree->insert(i);
        tree->insert(4);
        tree->insert(4);
    }

    void TearDown() override {
        delete tree;
    }
};

TEST_F(ContainsTests, SpecifiedNodeIsRoot) {
    ASSERT_TRUE(tree->contains(3));
}

TEST_F(ContainsTests, SpecifiedNodeNotInTree) {
    ASSERT_FALSE(tree->contains(8));
}

TEST_F(ContainsTests, SpecifiedNodeInLeftSubtree) {
    ASSERT_TRUE(tree->contains(1));
}

TEST_F(ContainsTests, SpecifiedNodeInRightSubtree) {
    ASSERT_TRUE(tree->contains(5));
}

TEST_F(ContainsTests, SpecifiedNodeIsLeaf) {
    ASSERT_TRUE(tree->contains(0));
}
